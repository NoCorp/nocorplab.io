---
title: Odebrecht
layout: campaign
header:
  title: Odebrecht
  summary: Odebrecht es una de las empresas atrapadas en Operacao Lava Jato, la investigación
    de corrupción de Brasil contra el gigante petrolero estatal Petrobras. Decenas
    de empresas reconocieron haber pagado sobornos a políticos y funcionarios a cambio
    de contratos con Petrobras.
  image: static/images/banner_plespiao-1.png
  icon: static/images/icon_play-circular.png
  link: ''
  thumbnail: static/images/odebrecht.jpeg
  showtitle: 'false'
  button: Sepa mas
resources:
  text: |-
    # Vista-se!

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu, aliquet tempus nisi. Donec tincidunt nisi ut erat mollis dignissim nec eget diam. Curabitur luctus elementum sem, id gravida metus porttitor et. Maecenas at nisi vel urna luctus sollicitudin ut sed nulla.
  left:
    title: Foto de perfil
    image: static/images/modelo_foto_perfil.png
  right:
    title: Capa de perfil
    image: static/images/modelo_capa_perfil.png
  link: ''
partners:
  text: |-
    # Pressione mais

    #### Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu, aliquet tempus nisi.
  item:
  - text: |-
      #### Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque.
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu.
    image: static/images/logos/logo_eff_fundo.png
    link: ''
  - text: |-
      ####  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque.
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu.
    image: static/images/logos/logo_avaaz_fundo.png
    link: ''
boxes:
  text: |-
    # Na mídia

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu, aliquet tempus nisi.
  category: midia
  max-posts: '9'
---

# Relatório da CPI dos crimes cibernéticos
                       
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipi.
                                
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu, aliquet tempus nisi. Donec tincidunt nisi ut erat mollis dignissim nec eget diam. Curabitur luctus elementum sem, id gravida metus porttitor et. Maecenas at nisi vel urna luctus sollicitudin ut sed nulla. Aliquam eleifend scelerisque ante, at scelerisque velit pretium eu. Nullam sed dui nec orci molestie elementum non vel lacus. Quisque felis ipsum, ultricies et viverra et, faucibus ac nunc. Ut quis consectetur ante. Quisque blandit ipsum laoreet bibendum pretium. Praesent in magna sit amet lacus molestie convallis. Vestibulum id lacus non nulla ornare maximus vitae eget lorem. Cras condimentum quam sit amet nulla gravida, in ultricies ex pellentesque.
                                
#### Se aprovada, a lei irá:
                                
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis dictum nisl. Vestibulum porta malesuada suscipit. Nullam est nisi, scelerisque sed mi eu, aliquet tempus nisi. Donec tincidunt nisi ut erat mollis dignissim nec eget diam. Curabitur luctus elementum sem, id gravida metus porttitor et. Maecenas at nisi vel urna luctus sollicitudin ut sed nulla. Aliquam eleifend scelerisque ante, at scelerisque velit pretium eu. Nullam sed dui nec orci molestie elementum non vel lacus. Quisque felis ipsum, ultricies et viverra et, faucibus ac nunc. Ut quis consectetur ante. Quisque blandit ipsum laoreet bibendum pretium. Praesent in magna sit amet lacus molestie convallis. Vestibulum id lacus non nulla ornare maximus vitae eget lorem. Cras condimentum quam sit amet nulla gravida, in ultricies ex pellentesque.
