var gulp = require('gulp'),
    gutil = require('gulp-util');
var $    = require('gulp-load-plugins')();

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/foundation-icon-fonts',
  'bower_components/motion-ui/src',
  'bower_components/font-awesome/scss'
];

var fontsPaths = [
  'bower_components/foundation-icon-fonts',
  'bower_components/font-awesome/fonts'
];

gulp.task('fonts', function() {
   gulp.src('bower_components/foundation-icon-fonts/*.{ttf,woff,eof,svg}' )
   .pipe(gulp.dest('css'));
   gulp.src('bower_components/font-awesome/fonts/*.{ttf,woff,eof,svg}' )
   .pipe(gulp.dest('fonts/font-awesome'));
});

gulp.task('sass', function() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));
});



gulp.task('default', ['sass', 'fonts'], function() {
  //gulp.build(['scss/**/*.scss'], ['sass']);

});


