#Mobilize-CDR
Plataforma de Mobilização da Coalizão Direitos na Rede

## Introdução
Mobilize-CDR é uma plataforma web responsiva construida a partir das mais modernas ferramentas de desevolvimento web disponíveis no momento. Entre as principais tecnologias utilizadas:

- Foundation Zurb = 6.0 (with SASS)
- Jekyll = 3.3.0
- jQuery = 2.2.0
- Owl-carousel
- Font-awesome
 
 As seguintes instruções tem como objetivo descrever os principais passos para instalação e utilização da plataforma em um ambiente que atenda os pré-requisitos informados. 

## Pré-requisitos
Para instalação e utilização da plataforma em um ambiente web, são necessáros que as seguintes ferramentas estejam pré-instalados no sistema:

- git >= 2.1.4
- nodejs >= 6.8.0
- npm >= 3.10.8
- ruby >= 2.1.0
- gem >2.2.2

Caso necessite de um ambiente pronto que atenda os pré-requisitos de instalação e desenvolvimeto da plataforma, é possível utilizar o seguinte container em Docker preparado para o desenvolvimento deste projeto:

https://hub.docker.com/r/thiagopaixao/node-ruby/

## Instalação


#### Clonando o repositório
Antes de tudo, devemos baixar o código-fonte do projeto. Para isso podemos baixar o arquivo .zip ou clonar o repositório com o seguinte comando:

`git clone https://gitlab.com/direitosnarede/mobilize-cdr.git`

`cd mobilize-cdr`

#### Instalando dependências
A seguinte sequência de comando deverá instalar todas as dependências do n osso projeto:

`npm install`

`bower install`

`npm start`

#### Gerando projeto
O comando a seguir criará o diretório **"_site"**  contendo a estrutura estática do projeto, pronto para ser publicado na pasta pública do servidor web:

`npm run build`
 
## Utilização
 Para um uso dinâmico, permitindo a publicação de conteúdo sem a necessidade de alteração do código-fonte, a atual versão da plataforma foi desenvolvida utilizando o [Jekyll](https://jekyllrb.com/ Jekyll), um gerador de sites estáticos. Para facilitar a alteração e publicação de conteúdos, é possível utilizar o Jekyll-admin com painel administrativo da plataforma. 

#### Executando o Jekyll
Para executar o jekyll-admin com saída de log no terminal, basta usar o jekyll server:

`bundle exec jekyll serve`

Para criar uma execução permanente e indepente da sessão atual, execute o jekyll utilizando o comando nohup. O parâmetro "-d" indica o local onde você quer que seja gerado o site estático, que pode ser diretamente no diretório público do servidor web.

`nohup bundle exec jekyll serve -d /var/www/html/ > ~/.mobilize.log &`

#### Acessando o Jekyll-admin (painel administrativo)
Acesse a seguinte URL em seu navegador, onde 'localhost' deve ser substituido pelo IP do seu servidor.

`http://localhost:4000/admin`

## Contribuição
Deseja contribuir para o projeto? Por favor, leia [CONTRIBUTING.md](CONTRIBUTING.md) para obter detalhes sobre o processo de desenvolvimento, nosso código de conduta sobre e o processo de envio de solicitações de pull requests.

## Licença
Este projeto é licenciado sob a licença GPLv3 - consulte o arquivo [LICENSE.md](LICENSE.md) para obter detalhes

## Autores
* **Thiago Paixão** - *Desenvolvedor* - [tap@thiagopaixao.com](mailto:tap@thiagopaixao.com)
