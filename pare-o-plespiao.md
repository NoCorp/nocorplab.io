---
title: Para o PL Espião
layout: page
header:
  title: Pare o PL Espião!
  summary: O PL 215/2015 que criminaliza a crítica aos políticos e legaliza a espionagem
    está prestes a ser votado na CCJC da Câmara dos Deputados. Denuncie.
  image: ''
  icon: "/static/images/icone_pcinvadido.png"
  thumbnail: http://i.imgur.com/jVwYIV6.png
partners:
  text: "# Pressione mais\nEntidades da sociedade civil reunidas na Articulação Marco
    Civil Já e na Coalizão Direitos na Rede repudiam as iniciativas parlamentares
    que atentam contra a privacidade e a liberdade de expressão. "
permalink: "/c/pare-o-plespiao/"
---

# O Brasil construiu uma das melhores leis para Internet do Mundo. Impeça o Congresso de aprovar a Pior

O Marco Civil da Internet é a carta de direitos para a era digital construída com participação ampla e intensa da população. Menos de um ano se passou desde a sua  regulamentação e alguns políticos planejam a sua destruição. 

O [**projeto de lei 215/2015**](http://www.camara.gov.br/proposicoesWeb/fichadetramitacao?idProposicao=946034) permite aos que estão no poder censurar a Internet e exige que os internautas brasileiros entreguem  seus dados pessoais ao acessar qualquer aplicativo de celular ou site.  

Durante sua tramitação em 2015, graças à mobilização pública sobre o PL 215, o "PL Espião",  algumas das piores medidas foram consertadas. Em uma de suas versões, o PL concedia à autoridades governamentais o acesso ao conteúdo das comunicações e aos registros de conexão de provedores de Internet e companhias telefônicas sem ordem judicial. Esta mobilização não pode parar.

 **A proposta de lei sobre crimes contra a honra aprovada pela Comissão de Constituição e Justiça ainda contém medidas que podem restringir profundamente o caráter livre e aberto da Internet no Brasil.**

**Se aprovada, a lei irá**:

- Amplia a retenção de dados obrigatória já determinada pelo Marco Civil da Internet: com o PL 215/2015, o Marco Civil passaria a obrigar todas as empresas de Internet a exigir nome, endereço completo, telefone, e-mail e CPF para você usar a Internet

- Permitir que qualquer pessoa solicite a exclusão de conteúdos que  considere ofensivos à sua honra ou que os associe a um crime de que  tenha sido absolvido. A medida afetará desde sites e posts nas redes  sociais, até as versões online de grandes jornais e arquivos digitais.

Nenhuma dessas duas medidas possui qualquer garantia à liberdade de  expressão e ao direito à informação. Os poderosos podem usar essa lei  para apagar a história e perseguir defensores de direitos humanos. 

Os parlamentares estão tentando apressar a aprovação deste projeto: **eles precisam ouvir sua opinião. Diga ao Congresso para votar Não.**

