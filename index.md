---
title: No Corp Today
layout: home
columns:
  text: "# La privatización de la democracia"
  left:
  - |-
    #### Empresas transnacionales y Planes de Acción Nacionales sobre Empresas y Derechos Humanos
    Existen distintos enfoques para lograr una mayor protección de los derechos humanos en el ámbito empresarial. Entre los instrumentos más importantes que se discuten actualmente se encuentran los Planes de Acción Nacionales (PAN) desarrollados en base a los Principios Rectores de la ONU ¡Mirá el video para aprender más sobre este tema!
  right:
  - '<iframe width="420" height="240" src="https://www.youtube.com/embed/lUzxg6dlnN8"
    frameborder="0" allowfullscreen></iframe> '
list:
  text: "# 5 Dimensiones da Captura Corporativa"
svg:
  text: "# 10 Razones para terminar"
columns2:
  text: "# El custo de la captura"
  left: "![Image1](static/images/costo_1.png)  ![Image2](static/images/costo_2.png)"
  right: "![Image3](static/images/costo_4.png)   ![Imagei4](static/images/costo_3.png)"
contact:
  text: "# Contato"
  left:
  - |-
    #### ¿Quieres compartir una idea, una acción o una sugerencia?
    contato@no-corp.org
  right:
  - |-
    #### Imprensa:
    imprensa@no-corp.org
follow:
  text: "# Siguenos"
  social:
  - title: Twitter
    icon: 'fi-social-twitter '
    link: "#"
  - title: Facebook
    icon: fi-social-facebook
    link: "#"
  - title: YouTube
    icon: fi-social-youtube
    link: "#"
slide_show:
- name: Captura
  image: static/images/banner_captura-1.png
  link: "#"
- name: Evasion Fiscal
  image: static/images/banner_captura-2.png
  link: "#"
---

**¿Por qué #NoCorp?**

El concepto “Captura Corporativa” surge poco en las conversaciones cotidianas de las personas, y tampoco es nombrado de forma explícita en los medios de comunicación tradicionales y populares. Sin embargo, este término nombra a las nuevas relaciones político-económicas entre los estados y las empresas transnacionales. 

<a href="/p/captura-corporativa/" role="tab" class="button" id="panel11a-heading" target="_blank" aria-controls="panel11a">¿Qué es?</a>
