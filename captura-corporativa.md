---
title: ¿Por qué #NoCorp? 
layout: page
permalink: "/p/captura-corporativa/"
---


{: .text-center}
El concepto “Captura Corporativa” surge poco en las conversaciones cotidianas de las personas, y tampoco es nombrado de forma explícita en los medios de comunicación tradicionales y populares. Sin embargo, este término nombra a las nuevas relaciones político-económicas entre los estados y las empresas transnacionales. 

{: .text-center}
Éstas están marcadas por la compra-venta de voluntades para lograr que estas respondan a los intereses de los grandes capitales con el fin de promover de manera continuada sus privilegios.

{: .text-center}
Es indispensable comprender cada uno de los mecanismos utilizados por las empresas transnacionales para obtener beneficios. Operan a nivel político, económico, social y mediático, todas se encuentran estrechamente relacionadas. Solamente a través de la información podremos dimensionar cómo estas estrategias producen en conjunto la pérdida del derecho a una vida digna. Por eso, decidimos organizarnos y utilizar la tecnología a
nuestro favor.

{: .text-center}
Durante seis meses hemos trabajado en una campaña digital integral que llegue públicos amplios y que explique puntualmente a qué refiere la Captura Corporativa. A través de la utilización de audiovisuales, gifs e infografías pretendemos dar a conocer casos en diferentes países latinoamericanos que ejemplifiquen las dimensiones de las opacas complicidades entre estados y empresas transnacionales y cómo estas limitan el acceso a una vida digna para todas y todos. Tenemos la
convicción compartida de que, a través de la comprensión profunda de este fenómeno, nuevos esfuerzos organizativos y populares pueden surgir para
accionar colectivamente y lograr frenar de manera creativa -desde el consumo, la protesta y la participación política- el avance de la captura corporativa del estado, de la democracia en
sí misma.
